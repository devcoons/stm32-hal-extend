#ifndef LBR_FRAMEWORK_H_

    #define LBR_FRAMEWORK_H_
    
    #include <string.h>
    #include <inttypes.h>
    #include "cmsis_os.h"
    #include "FreeRTOS.h"
    #include "task.h"

    typedef enum
    {
    	I_OK = 0x00,
    	I_ERROR = 0x01,
    	I_BUSY = 0x02,
    	I_TIMEOUT = 0x03,
    	I_INVALID = 0x04,
    	I_WARNING = 0x05,
    	I_REFRESHED = 0x06,
    	I_VALID_TRUSTED = 0x07,
    	I_VALID_UNTRUSTED = 0x08,
	    I_INVALID_TRUSTED = 0x09,
        I_INVALID_UNTRUSTED = 0x10
    }
    I_Status;
    
    typedef void (*I_FunctionPtr)();

#endif