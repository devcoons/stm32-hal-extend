/******************************************************************************
 * Includes
 ******************************************************************************/

#include <lbr_threads.h>

/******************************************************************************
 * Global Variables
 ******************************************************************************/

/******************************************************************************
 * Functions
 ******************************************************************************/

/*
 * @func   isca_framework_thread_init
 * @brief  <missing>
 * @param  <missing>
 * @retval void
 */

void lbr_thread_init(thread_info_t * info, THREAD_TYPE type,THREAD_MODE mode)
{
	// Initialize thread info block

	info->condition = TC_RUNNING;
	info->state = TS_INACTIVE;
	info->type = type;
	info->mode = mode;

	// Wait for TC_RUNNING Signal from parent thread

	lbr_thread_barrier(info, TS_INITIALIZE);
}

/*
 * @func   isca_framework_thread_execute
 * @brief  <missing>
 * @param  <missing>
 * @retval void
 */

void lbr_thread_execute(thread_info_t * info)
{
	lbr_thread_barrier(info, TS_ACTIVE);
}

/*
 * @func   isca_framework_thread_deinit
 * @brief  <missing>
 * @param  <missing>
 * @retval void
 */

void lbr_thread_deinit(thread_info_t * info, osThreadId * handler)
{
	info->state = TS_INACTIVE;
	info->condition = TC_WAITING;

	if (handler != NULL)	vTaskDelete (NULL);
}

/*
 * @func   isca_framework_thread_barrier
 * @brief  <missing>
 * @param  <missing>
 * @retval void
 */

void lbr_thread_barrier(thread_info_t * task, THREAD_STATE nextState)
{
	task->condition = TC_WAITING;

	if (task->mode == TM_MANUAL)
		while (task->condition != TC_WAITING)
			vTaskDelay(53);
	else
		task->condition = TC_RUNNING;

	task->state = nextState;
}

uint8_t lbr_thread_toggle(char * name, void (*functionPtr)(const void *), thread_info_t * thread_info,	osThreadId * thread_handle,void * arguments)
{
	int timeout = 250;

	if (thread_info->state == TS_INACTIVE)
	{
		osThreadDef_t os_thread_def = { name, functionPtr, osPriorityNormal, 0, 128 };
		*thread_handle = osThreadCreate(&os_thread_def, arguments);

		do
		{
			thread_info->condition = TC_RUNNING;
			for (int j = 0; j < 4 * 2; j++)
			{
				BSP_LED_Toggle((Led_TypeDef)(j % 4));
				HAL_Delay(25);
			}

			if(thread_info->state == TS_ERROR_ALL)
			{
				for (int j = 7; j >= 0; j--)
				{
					BSP_LED_Toggle((Led_TypeDef)(j % 4));
					HAL_Delay(25);
				}
				return 0x01;
			}
		} while (thread_info->state != TS_ACTIVE);
	}
	else
	{
		thread_info->state = TS_DEINITIALIZE;

		do
		{
			timeout--;
			for (int j = 0; j < 4 * 2; j++)
			{
				BSP_LED_Toggle((Led_TypeDef)(j % 4));
				HAL_Delay(25);
			}
			if(thread_info->state == TS_ERROR_ALL)
			{
				for (int j = 0; j < 4; j++)
					BSP_LED_Off((Led_TypeDef)(j));

				for (int j = 7; j >= 0; j--)
				{
					BSP_LED_Toggle((Led_TypeDef)(j % 4));
					HAL_Delay(25);
				}
				return 0x01;
			}
			if (timeout <= 0)return 0x01;
		}while (thread_info->state != TS_INACTIVE);
	}

	for (int j = 0; j < 4; j++)
		BSP_LED_Off((Led_TypeDef)(j));

	return 0x00;
}

uint8_t lbr_thread_start(char * name, void (*functionPtr)(const void *), thread_info_t * thread_info, osThreadId * thread_handle, const void * arguments)
{
	if (thread_info->state == TS_INACTIVE)
	{
		osThreadDef_t os_thread_def = { name, functionPtr, osPriorityNormal, 0, 128 };
		*thread_handle = osThreadCreate(&os_thread_def, arguments);

		do
		{
			thread_info->condition = TC_RUNNING;
			for (int j = 0; j < 4 * 2; j++)
			{
				BSP_LED_Toggle((Led_TypeDef)(j % 4));
				HAL_Delay(25);
			}

			if(thread_info->state == TS_ERROR_ALL)
			{
				for (int j = 7; j >= 0; j--)
				{
					BSP_LED_Toggle((Led_TypeDef)(j % 4));
					HAL_Delay(25);
				}
				return 0x01;
			}
		} while (thread_info->state != TS_ACTIVE);
	}

	return 0x00;
}

uint8_t lbr_thread_stop(thread_info_t * thread_info)
{
	int timeout = 250;

	if (thread_info->state != TS_INACTIVE)
	{
		thread_info->state = TS_DEINITIALIZE;

		do
		{
			timeout--;
			for (int j = 0; j < 4 * 2; j++)
			{
				BSP_LED_Toggle((Led_TypeDef)(j % 4));
				HAL_Delay(25);
			}
			if(thread_info->state == TS_ERROR_ALL)
			{
				for (int j = 0; j < 4; j++)
					BSP_LED_Off((Led_TypeDef)(j));

				for (int j = 7; j >= 0; j--)
				{
					BSP_LED_Toggle((Led_TypeDef)(j % 4));
					HAL_Delay(25);
				}
				return 0x01;
			}
			if (timeout <= 0)return 0x01;
		}while (thread_info->state != TS_INACTIVE);
	}

	for (int j = 0; j < 4; j++)
		BSP_LED_Off((Led_TypeDef)(j));

	return 0x00;
}
